package client

import (
    "fmt"
    "net"
    "strconv"
    "sync"
    "time"

    "gitlab.com/kohlten/packet_parser"

    "gitlab.com/kohlten/game_server"
    "gitlab.com/kohlten/game_server/callbacks"
)

const maxPacketsPerCycle = 1000
const maxThreads = 10

type GameClient struct {
    address          *net.UDPAddr
    socket           *net.UDPConn
    packetQueue      []packet_parser.Packet
    data             []packet_parser.Packet
    parsed           []*packet_parser.PacketData
    packetsAvailable chan int
    packetMutex      *sync.Mutex
    dataMutex        *sync.Mutex
    running          bool
    sleepTime        uint16
    useCallbacks     bool
    callbackHandler  *callbacks.CallbackHandler
    formatter        callbacks.PacketFormatter
    parser           callbacks.PacketParser
    callbackMutex    *sync.Mutex
    parsedMutex      *sync.Mutex
    err              error
}

/* ------------------------------ BASE ------------------------------ */

// Creates a new client connected to the address and port. Sleeptime is how long to sleep after each iteration of the internal functions.
func NewGameClient(address string, port, sleepTime uint16) (*GameClient, error) {
    gClient := new(GameClient)
    addr, err := net.ResolveUDPAddr("udp", address+":"+strconv.FormatInt(int64(port), 10))
    if err != nil {
        return nil, err
    }
    gClient.address = addr
    gClient.socket, err = net.DialUDP("udp", nil, gClient.address)
    if err != nil {
        return nil, err
    }
    gClient.running = true
    gClient.packetsAvailable = make(chan int)
    gClient.packetMutex = &sync.Mutex{}
    gClient.dataMutex = &sync.Mutex{}
    gClient.sleepTime = sleepTime
    go gClient.dataListener()
    go gClient.dataWriter()
    return gClient, nil
}

func NewGameClientCallbacks(address string, port, sleepTime uint16, formatter callbacks.PacketFormatter, parser callbacks.PacketParser) (*GameClient, error) {
    gClient, err := NewGameClient(address, port, sleepTime)
    if err != nil {
        return nil, err
    }
    gClient.useCallbacks = true
    gClient.formatter = formatter
    gClient.parser = parser
    gClient.callbackHandler = callbacks.NewCallbackHandler(gClient, true)
    gClient.callbackMutex = &sync.Mutex{}
    gClient.parsedMutex = &sync.Mutex{}
    go gClient.update()
    return gClient, nil
}

// Sends a packet to the client on the address
func (gClient *GameClient) SendPacket(packet []byte) {
    gClient.packetMutex.Lock()
    gClient.packetQueue = append(gClient.packetQueue, packet_parser.Packet{Data: packet, Addr: nil, ReceiveTime: 0})
    gClient.packetMutex.Unlock()
    gClient.packetsAvailable <- 1
}

// Returns a packet from the list of parsed packets. If the list is empty, it will return nil.
func (gClient *GameClient) GetParsedPacket() *packet_parser.PacketData {
    if len(gClient.parsed) == 0 {
        return nil
    }
    gClient.parsedMutex.Lock()
    packet := gClient.parsed[len(gClient.parsed)-1]
    gClient.parsed = gClient.parsed[:len(gClient.parsed)-1]
    gClient.parsedMutex.Unlock()
    return packet
}

// Returns a packet from the list of packets. If the list is empty, it will return nil.
func (gClient *GameClient) GetPacket(loc uint) *packet_parser.Packet {
    if len(gClient.data) == 0 || loc >= uint(len(gClient.data)) {
        return nil
    }
    gClient.dataMutex.Lock()
    packet := gClient.data[loc]
    gClient.data = gClient.data[:len(gClient.data)-1]
    gClient.dataMutex.Unlock()
    return &packet
}

func (gClient *GameClient) PeekPacket(loc uint) *packet_parser.Packet {
    if len(gClient.data) == 0 || loc >= uint(len(gClient.data)) {
        return nil
    }
    gClient.dataMutex.Lock()
    packet := gClient.data[loc]
    gClient.dataMutex.Unlock()
    return &packet
}

// Returns the current available packets that have been parsed
func (gClient *GameClient) AvailableParsedPackets() uint64 {
    return uint64(len(gClient.parsed))
}

// Returns the current available packets that have not been parsed
func (gClient *GameClient) AvailablePackets() uint64 {
    return uint64(len(gClient.data))
}

// Returns the current IPv4 address in the format of 000.000.000.000:0000
func (gClient *GameClient) GetStringAddress() string {
    return gClient.address.String()
}

func (gClient *GameClient) GetAddress() *net.UDPAddr {
    return gClient.address
}

// Closes the gClient and stops the internal functions
func (gClient *GameClient) Close() {
    for len(gClient.packetQueue) > 0 {
        time.Sleep(time.Millisecond * 10)
    }
    time.Sleep(5 * time.Millisecond)
    gClient.packetsAvailable <- 1
    gClient.running = false
    gClient.socket.Close()
}

// Returns an error if there was one, otherwise will return nil. If there was an error, it will also reset the internal error.
func (gClient *GameClient) GetError() error {
	err := gClient.err
	gClient.err = nil
	return err
}

/* ------------------------------ CALLBACKS ------------------------------ */

func (gClient *GameClient) AddCallback(action uint8, function callbacks.CallbackFunction, data ...interface{}) {
    if !gClient.useCallbacks {
        return
    }
    gClient.callbackMutex.Lock()
    gClient.callbackHandler.AddCallback(action, function, data...)
    gClient.callbackMutex.Unlock()
}

func (gClient *GameClient) RemoveCallback(action uint8) {
    if !gClient.useCallbacks {
        return
    }
    gClient.callbackMutex.Lock()
    gClient.callbackHandler.RemoveCallback(action)
    gClient.callbackMutex.Unlock()
}

/* ------------------------------ INTERNAL ------------------------------ */

func (gClient *GameClient) packetUpdate(packets []packet_parser.Packet, wg *sync.WaitGroup) {
    for i := 0; i < len(packets); i++ {
        packet := packets[i]
        if packet.Data == nil || packet.Addr == nil || len(packet.Data) == 0 {
            continue
        }
        packetParsed, err := gClient.parser(packet.Data, packet.Addr)
        if err != nil {
            fmt.Println(err.Error())
            continue
        }
        gClient.callbackMutex.Lock()
        callback := gClient.callbackHandler.GetCallback(packetParsed.Action)
        if callback != nil {
            callback.Run(packetParsed)
        } else {
            gClient.parsedMutex.Lock()
            gClient.parsed = append(gClient.parsed, packetParsed)
            gClient.parsedMutex.Unlock()
        }
        gClient.callbackMutex.Unlock()
    }
    wg.Done()
}

func (gClient *GameClient) update() {
    wg := sync.WaitGroup{}
    for gClient.running {
        st := game_server.GetTimeMS()
        gClient.dataMutex.Lock()
        packetsBeingParsed := len(gClient.data)
        if packetsBeingParsed > 0 {
            if packetsBeingParsed > maxPacketsPerCycle {
                packetsBeingParsed = maxPacketsPerCycle
            }
            numPackets := packetsBeingParsed / maxThreads
            for i := 0; i < maxThreads; i++ {
                packets := make([]packet_parser.Packet, numPackets)
                copy(packets, gClient.data[i*numPackets:(i*numPackets)+numPackets])
                if i == maxThreads-1 && packetsBeingParsed%maxThreads != 0 {
                    for j := (i + 1) * numPackets; j < packetsBeingParsed; j++ {
                        packets = append(packets, gClient.data[j])
                    }
                }
                wg.Add(1)
                go gClient.packetUpdate(packets, &wg)
            }
            gClient.data = gClient.data[packetsBeingParsed:]
        }
        wg.Wait()
        gClient.dataMutex.Unlock()
        current := game_server.GetTimeMS()
        if current-st < uint64(gClient.sleepTime) {
            time.Sleep(time.Millisecond * time.Duration(uint64(gClient.sleepTime)-(current-st)))
        }
    }
}

func (gClient *GameClient) dataListener() {
    for gClient.running {
        _ = gClient.socket.SetReadDeadline(time.Now().Add(500 * time.Millisecond))
        data := make([]byte, 1024)
        dataAvailable, addr, err := gClient.socket.ReadFromUDP(data)
        if dataAvailable > 0 && addr != nil && err == nil {
            gClient.dataMutex.Lock()
            gClient.data = append(gClient.data, packet_parser.Packet{Data: data[:dataAvailable], Addr: addr, ReceiveTime: game_server.GetTimeMS()})
            gClient.dataMutex.Unlock()
        } else if err != nil && !err.(net.Error).Timeout() && gClient.running {
            gClient.err = err
        }
    }
}

func (gClient *GameClient) dataWriter() {
    for gClient.running {
        <-gClient.packetsAvailable
        gClient.packetMutex.Lock()
        for i := 0; i < len(gClient.packetQueue); i++ {
            packet := gClient.packetQueue[i]
            _, _ = gClient.socket.Write(packet.Data)
        }
        gClient.packetQueue = nil
        gClient.packetMutex.Unlock()
    }
}
