package callbacks

import (
	"gitlab.com/kohlten/packet_parser"
)

const UnusedPacket = 0xff

func NilCallback(*packet_parser.PacketData, ...interface{})  {}

type Callback struct {
	Action uint8
	Function CallbackFunction
	Data []interface{}
}

func NewCallback(action uint8, function CallbackFunction, data ...interface{}) *Callback {
	callback := new(Callback)
	callback.Action = action
	callback.Function = function
	callback.Data = data
	return callback
}

func (callback *Callback) Run(data *packet_parser.PacketData) {
	callback.Function(data, callback.Data...)
}




