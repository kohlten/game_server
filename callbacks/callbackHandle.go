package callbacks

import (
    "net"

    "gitlab.com/kohlten/packet_parser"
)

type PacketFormatter func(action uint8, a ...interface{}) []byte
type PacketParser func(packet []byte, addr *net.UDPAddr) (*packet_parser.PacketData, error)

type CallbackFunction func(*packet_parser.PacketData, ...interface{})

/*
GetPacket should remove the packet from the internal list
PeekPacket should just return the packet from the internal list
AvailablePackets should return the amount of packets that can be parsed
 */
type Socket interface {
    GetPacket(loc uint) *packet_parser.Packet
    PeekPacket(loc uint) *packet_parser.Packet
    AvailablePackets() uint64
}

type CallbackHandler struct {
    callbacks map[uint8]*Callback
    socket    Socket
    removeUnusedPackets bool
}

var unusedPacket = NewCallback(UnusedPacket, NilCallback)

func NewCallbackHandler(socket Socket, removeUnusedPackets bool) *CallbackHandler {
    handler := new(CallbackHandler)
    handler.callbacks = make(map[uint8]*Callback)
    handler.socket = socket
    handler.removeUnusedPackets = removeUnusedPackets
    return handler
}

func (handler *CallbackHandler) AddCallback(action uint8, function CallbackFunction, data ...interface{}) {
    callback := NewCallback(action, function, data...)
    handler.callbacks[action] = callback
}

func (handler *CallbackHandler) RemoveCallback(action uint8) {
    _, ok := handler.callbacks[action]
    if !ok {
        return
    }
    delete(handler.callbacks, action)
}

func (handler *CallbackHandler) GetCallback(action uint8) *Callback {
    callback, ok := handler.callbacks[action]
    if !ok {
        return unusedPacket
    }
    return callback
}

func (handler *CallbackHandler) Update() {
	length := handler.socket.AvailablePackets()
	for i := uint64(0); i < length; i++ {
		packet := handler.socket.PeekPacket(uint(i))
		parsed, err := packet_parser.ParsePacket(packet.Data, packet.Addr)
		if err != nil {
            if handler.removeUnusedPackets {
                handler.socket.GetPacket(uint(i))
            }
			continue
		}
		callback := handler.GetCallback(parsed.Action)
		if callback == unusedPacket {
		    if handler.removeUnusedPackets {
                handler.socket.GetPacket(uint(i))
            }
			continue
		}
		callback.Run(parsed)
		handler.socket.GetPacket(uint(i))
	}
}
