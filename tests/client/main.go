package main

import (
	"fmt"
	"gitlab.com/kohlten/game_server/client"
	"gitlab.com/kohlten/packet_parser"
	"sync"
)

const (
	JOIN = 0x81
	LEAVE = 0x82
)

var wg = sync.WaitGroup{}

func sendPackets(gClient *client.GameClient) {
	defer wg.Done()
	for i := 0; i < 10000; i++ {
		gClient.SendPacket(packet_parser.FormatPacket(0x1))
	}
}

func main() {
	gClient, err := client.NewGameClient("0.0.0.0", 9595, 33)
	if err != nil {
		panic(err)
	}
	gClient.SendPacket(packet_parser.FormatPacket(JOIN))
	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go sendPackets(gClient)
	}
	wg.Wait()
	for i := 0; i < 1; i++ {
		gClient.SendPacket(packet_parser.FormatPacket(LEAVE))
	}
	gClient.Close()
	fmt.Println("Finished")
}
