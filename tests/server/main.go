package main

import (
	"fmt"
	server2 "gitlab.com/kohlten/game_server/server"
	"gitlab.com/kohlten/packet_parser"
	"os"
	"time"
)

const (
	JOIN = 0x81
	LEAVE = 0x82
)

var st = uint64(0)
var done = false

func join(data *packet_parser.PacketData, items ...interface{}) {
	st = uint64(time.Now().UnixNano()) / uint64(time.Millisecond)
	fmt.Println("New client on address ", data.Addr.String())
}

func leave(data *packet_parser.PacketData, items ...interface{}) {
	fmt.Println("Took ",  uint64(time.Now().UnixNano()) / uint64(time.Millisecond) - st)
	fmt.Println("Removed client on address ", data.Addr.String())
	done = true
}

func main() {
	server, err := server2.NewGameServerCallbacks(9595, 16, packet_parser.FormatPacket, packet_parser.ParsePacket)
	if err != nil {
		os.Exit(1)
	}
	server.AddCallback(JOIN, join)
	server.AddCallback(LEAVE, leave)
	for !done {
		time.Sleep(1000 * time.Millisecond)
		packets := server.AvailableParsedPackets()
		for i := uint64(0); i < packets; i++ {
			_ = server.GetParsedPacket()
		}
	}
	server.Close()
}
