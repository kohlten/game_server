package game_server

import "time"

func GetTimeMS() uint64 {
	return uint64(time.Now().UnixNano()) / uint64(time.Millisecond)
}

func GetTimeNS() uint64 {
	return uint64(time.Now().UnixNano())
}