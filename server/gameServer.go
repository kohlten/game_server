package server

import (
	"fmt"
	"net"
	"strconv"
	"sync"
	"time"

	"gitlab.com/kohlten/packet_parser"

	"gitlab.com/kohlten/game_server"
	"gitlab.com/kohlten/game_server/callbacks"
)

const maxPacketsPerCycle = 5000
const maxThreads = 10

/*
@TODO Make the functions a looping function so we don't create so many threads
*/

// PacketMutex is probably not needed
type GameServer struct {
	address          *net.UDPAddr
	socket           *net.UDPConn
	formatter        callbacks.PacketFormatter
	parser           callbacks.PacketParser
	callbackHandler  *callbacks.CallbackHandler
	packetQueue      []packet_parser.Packet
	data             []packet_parser.Packet
	parsed           []*packet_parser.PacketData
	packetsAvailable chan int
	packetMutex      *sync.Mutex
	dataMutex        *sync.Mutex
	callbackMutex    *sync.Mutex
	parsedMutex      *sync.Mutex
	useCallbacks     bool
	running          bool
	sleepTime        uint16
	packets          uint64
	startTime        uint64
	iterations       uint64
	err error
}

/* ------------------------------ BASE ------------------------------ */

// Creates a new server on the user specified port. Sleeptime is how long to sleep after each iteration of the internal functions.
func NewGameServer(port, sleepTime uint16) (*GameServer, error) {
	gServer := new(GameServer)
	addr, err := net.ResolveUDPAddr("udp", "0.0.0.0:"+strconv.FormatInt(int64(port), 10))
	if err != nil {
		return nil, err
	}
	gServer.address = addr
	gServer.socket, err = net.ListenUDP("udp", gServer.address)
	if err != nil {
		return nil, err
	}
	gServer.running = true
	gServer.packetsAvailable = make(chan int)
	gServer.packetMutex = &sync.Mutex{}
	gServer.dataMutex = &sync.Mutex{}
	gServer.parsedMutex = &sync.Mutex{}
	gServer.sleepTime = sleepTime
	gServer.startTime = game_server.GetTimeMS()
	for i := 0; i < maxThreads; i++ {
		go gServer.dataListener()
		go gServer.dataWriter()
	}
	return gServer, nil
}

// Creates a new server with callbacks. Formatter is the function to format each of the packets.
// Parser is the function to parse the packets.
func NewGameServerCallbacks(port, sleepTime uint16, formatter callbacks.PacketFormatter, parser callbacks.PacketParser) (*GameServer, error) {
	gServer, err := NewGameServer(port, sleepTime)
	if err != nil {
		return nil, err
	}
	gServer.callbackHandler = callbacks.NewCallbackHandler(gServer, true)
	gServer.formatter = formatter
	gServer.parser = parser
	gServer.useCallbacks = true
	gServer.callbackMutex = &sync.Mutex{}
	for i := 0; i < maxThreads; i++ {
		go gServer.update()
	}
	return gServer, nil
}

// Sends a packet to the client on the address
func (gServer *GameServer) SendPacket(addr *net.UDPAddr, packet []byte) {
	gServer.packetMutex.Lock()
	gServer.packetQueue = append(gServer.packetQueue, packet_parser.Packet{Data: packet, Addr: addr, ReceiveTime: 0})
	gServer.packetMutex.Unlock()
	gServer.packetsAvailable <- 1
}

// Returns a packet from the list of parsed packets. If the list is empty, it will return nil.
func (gServer *GameServer) GetParsedPacket() *packet_parser.PacketData {
	if len(gServer.parsed) == 0 {
		return nil
	}
	gServer.parsedMutex.Lock()
	packet := gServer.parsed[len(gServer.parsed)-1]
	gServer.parsed = gServer.parsed[:len(gServer.parsed)-1]
	gServer.parsedMutex.Unlock()
	return packet
}

// Returns a packet from the list of packets. If the list is empty, it will return nil.
func (gServer *GameServer) GetPacket(loc uint) *packet_parser.Packet {
	if len(gServer.data) == 0 || loc >= uint(len(gServer.data)) {
		return nil
	}
	gServer.dataMutex.Lock()
	packet := gServer.data[loc]
	gServer.data = gServer.data[:len(gServer.data)-1]
	gServer.dataMutex.Unlock()
	return &packet
}

func (gServer *GameServer) PeekPacket(loc uint) *packet_parser.Packet {
	if len(gServer.data) == 0 || loc >= uint(len(gServer.data)) {
		return nil
	}
	gServer.dataMutex.Lock()
	packet := gServer.data[loc]
	gServer.dataMutex.Unlock()
	return &packet
}

func (gServer *GameServer) AvailableParsedPackets() uint64 {
	return uint64(len(gServer.parsed))
}

func (gServer *GameServer) AvailablePackets() uint64 {
	return uint64(len(gServer.data))
}

func (gServer *GameServer) GetAddress() string {
	return gServer.address.String()
}

// Closes the server and stops the internal functions
func (gServer *GameServer) Close() {
	gServer.running = false
	gServer.packetsAvailable <- 1
	gServer.socket.Close()
}

// Returns an error if there was one, otherwise will return nil. If there was an error, it will also reset the internal error.
func (gServer *GameServer) GetError() error {
	err := gServer.err
	gServer.err = nil
	return err
}


/* ------------------------------ CALLBACKS ------------------------------ */

// Adds a new callback to the server. The function of the callback will be called every time there is a packet with the type of the action.
func (gServer *GameServer) AddCallback(action uint8, function callbacks.CallbackFunction, data ...interface{}) {
	if !gServer.useCallbacks {
		return
	}
	gServer.callbackMutex.Lock()
	gServer.callbackHandler.AddCallback(action, function, data...)
	gServer.callbackMutex.Unlock()
}

func (gServer *GameServer) RemoveCallback(action uint8) {
	if !gServer.useCallbacks {
		return
	}
	gServer.callbackMutex.Lock()
	gServer.callbackHandler.RemoveCallback(action)
	gServer.callbackMutex.Unlock()
}

/* ------------------------------ INTERNAL ------------------------------ */

func (gServer *GameServer) dataListener() {
	for gServer.running {
		_ = gServer.socket.SetReadDeadline(time.Now().Add(500 * time.Millisecond))
		data := make([]byte, 1024)
		dataAvailable, addr, err := gServer.socket.ReadFromUDP(data)
		if dataAvailable > 0 && addr != nil && err == nil {
			gServer.dataMutex.Lock()
			gServer.data = append(gServer.data, packet_parser.Packet{Data: data[:dataAvailable], Addr: addr, ReceiveTime: game_server.GetTimeMS()})
			gServer.dataMutex.Unlock()
		} else if err != nil && !err.(net.Error).Timeout() {
			gServer.err = err
		}
	}
}

func (gServer *GameServer) dataWriter() {
	for gServer.running {
		<-gServer.packetsAvailable
		gServer.packetMutex.Lock()
		for i := 0; i < len(gServer.packetQueue); i++ {
			packet := gServer.packetQueue[i]
			_, _ = gServer.socket.WriteToUDP(packet.Data, packet.Addr)
		}
		gServer.packetQueue = nil
		gServer.packetMutex.Unlock()
	}
}

func (gServer *GameServer) packetUpdate(packets []packet_parser.Packet, wg *sync.WaitGroup) {
	for i := 0; i < len(packets); i++ {
		packet := packets[i]
		if packet.Data == nil || packet.Addr == nil || len(packet.Data) == 0 {
			continue
		}
		packetParsed, err := gServer.parser(packet.Data, packet.Addr)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		gServer.callbackMutex.Lock()
		callback := gServer.callbackHandler.GetCallback(packetParsed.Action)
		if callback != nil {
			callback.Run(packetParsed)
		} else {
			gServer.parsedMutex.Lock()
			gServer.parsed = append(gServer.parsed, packetParsed)
			gServer.parsedMutex.Unlock()
		}
		gServer.callbackMutex.Unlock()
	}
	wg.Done()
}

// Separate this function
func (gServer *GameServer) update() {
	wg := sync.WaitGroup{}
	for gServer.running {
		st := game_server.GetTimeMS()
		gServer.dataMutex.Lock()
		packetsBeingParsed := len(gServer.data)
		if packetsBeingParsed > 0 {
			if packetsBeingParsed > maxPacketsPerCycle {
				packetsBeingParsed = maxPacketsPerCycle
			}
			numPackets := packetsBeingParsed / maxThreads
			for i := 0; i < maxThreads; i++ {
				packets := make([]packet_parser.Packet, numPackets)
				copy(packets, gServer.data[i*numPackets:(i*numPackets)+numPackets])
				if i == maxThreads-1 && packetsBeingParsed%maxThreads != 0 {
					for j := (i + 1) * numPackets; j < packetsBeingParsed; j++ {
						packets = append(packets, gServer.data[j])
					}
				}
				wg.Add(1)
				go gServer.packetUpdate(packets, &wg)
			}
			gServer.data = gServer.data[packetsBeingParsed:]
		}
		wg.Wait()
		gServer.dataMutex.Unlock()
		current := game_server.GetTimeMS()
		if current-st < uint64(gServer.sleepTime) {
			time.Sleep(time.Millisecond * time.Duration(uint64(gServer.sleepTime)-(current-st)))
		}
	}
}
